using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public struct Resettle_cost {
    public int Pop;
    public int Food;
    public int Material;
};

public class GameManager : MonoBehaviour {
    public static GameManager Instance{ set; get;}

    // public BuildingConfig buildingConfig;
    
    private float timeofnextday = 0;

    public int day = 0;
    public int dayOnSpot = 0;

    public TMP_Text dayText, populationText, foodText, materialText;
    public GameObject WinScreen, LoseScreen, Pausemenu, resettleButton;

    public AudioSource MainMusicPlayer;

    public Canvas EventUI;


    public List<Stage> stages;
    private int activeStage = 0;

    private float resettle_efficiency = 0.25f;

    private EventBase currentEvent;

    private void Awake () {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (this.gameObject);
        } else {
            Destroy (gameObject);
        }
    }

    private void Start(){
        timeofnextday = Time.time;

        // invoke setters at least once, to force display update
        Pop += 0;
        Food += 0;
        Material += 0;

        foreach(Stage stage in stages){
            stage.gameObject.SetActive(false);
        }
        stages[0].gameObject.SetActive(true);
        resettleButton.SetActive(false);
    }


    private void Update() {
        float now = Time.time;

        if (( now >= timeofnextday)) {
            // it's a new day
            day += 1;
            timeofnextday += 5f;
            dayOnSpot += 1;

            DayEvent();


            if (Pop <= 0) {
                // GameOver
                GameOverLose();
                // PauseGame();
                // LoseScreen.gameObject.SetActive(true);
            }

        }

    }


    private void DayEvent(){
        // for every building, resource income?
        dayText.text = "" + day + " (" + dayOnSpot + ")" ;

        Food -= Pop;
        if (Pop > Pop_cap) Food -= (Pop - Pop_cap);
        if (Food <= 0) {
            Pop += Food;
            Food = 0;
        }

        // check for critical events to throw
        if (Pop > 200) {
            ShowEvent(new OverPopEvent());
        }
        
        if (Food < 5) {
            ShowEvent(new NoFoodLeftEvent());
        }

        if (day == 1) {
            ShowEvent(new IntroductionEvent());
        }
        // check for stage specific events to throw
        stages[activeStage].triggerDayEvent();


        Resettle_cost cost = stages[activeStage].resettle_cost;

        if (
            Pop < cost.Pop ||
            Food < cost.Food ||
            Material < cost.Material
        ) {
            // not ready yet
            resettleButton.SetActive(false);
        } else {
            // activate the buttle
            resettleButton.SetActive(true);
        }

        
        
    }


    public void resettle(){
        Resettle_cost cost = stages[activeStage].resettle_cost;
        // check if enough stuff available
        if (
            Pop < cost.Pop ||
            Food < cost.Food ||
            Material < cost.Material
        ) {
            // complain to player that they aren't ready, but they shouldn't:tm: be able to trigger this yet anyway
            return;
        }

        // reduce materials
        Pop -= cost.Pop;
        Food -= cost.Food;
        Material -= cost.Material;

        // take only what you can carry, efficiency loss
        Pop = (int) Mathf.Floor(Pop * resettle_efficiency);
        Food = (int) Mathf.Floor(Food * resettle_efficiency);
        Material = (int) Mathf.Floor(Material * resettle_efficiency);

        // Switch active Spot

        stages[activeStage].gameObject.SetActive(false);
        activeStage += 1;
        dayOnSpot = 0;
        if (activeStage >= stages.Count){
            // game over
            GameOverWin();
            // PauseGame();
            // WinScreen.gameObject.SetActive(true);
            return;
        }
        stages[activeStage].gameObject.SetActive(true);
        stages[activeStage].triggerDayEvent();
    }


    public void ShowEvent(EventBase e){
        currentEvent = e;
        PauseGame();
        EventUI.gameObject.SetActive(true);

        // populate UI
        EventUI.gameObject.transform.Find("Title").GetComponent<TMP_Text>().text = e.title;
        EventUI.gameObject.transform.Find("Description").GetComponent<TMP_Text>().text = e.fulldescription;
        

        Button choice1 = EventUI.gameObject.transform.Find("Panel").Find("Choice1").GetComponent<Button>();
        Button choice2 = EventUI.gameObject.transform.Find("Panel").Find("Choice2").GetComponent<Button>();

        // choice1.gameObject.GetComponent<TMP_Text>().text = e.choice1text;

        // GameObject test = EventUI.gameObject.transform.Find("choice1UI_text").gameObject;

        EventUI.gameObject.transform.Find("Panel")
            .Find("Choice1").gameObject.transform
            .Find("choice1UI_text").GetComponent<TMP_Text>().text = e.choice1text;

        // test.GetComponent<TMP_Text>().text = "test";
        //  = "test text "; //e.choice1text;

        if (e.isChoice){
            EventUI.gameObject.transform.Find("Panel")
                .Find("Choice2").gameObject.transform
                .Find("choice2UI_text").GetComponent<TMP_Text>().text = e.choice2text;
            choice2.gameObject.SetActive(true);
        } else {
            choice2.gameObject.SetActive(false);
        }

        Image eSprite = EventUI.gameObject.transform.Find("EventImage").GetComponent<Image>();
        if (e.EventSprite){
            eSprite.sprite = e.EventSprite;
            eSprite.gameObject.SetActive(true);
        } else {
            eSprite.gameObject.SetActive(false);
        }

    }

    public void hideEvent(){
        UnPauseGame();
        EventUI.gameObject.SetActive(false);

    }

    // event response hooks for GameObjects/Buttons
    public void eventAccept(){
        hideEvent();
        currentEvent.resolveEvent(true);
    }
    public void eventReject(){
        hideEvent();
        currentEvent.resolveEvent(false);
    }

    public void PauseGame(){
        Time.timeScale = 0;
        if (MainMusicPlayer) MainMusicPlayer.volume = 0.5f;
        Pausemenu.gameObject.SetActive(true);
        // MainMusicPlayer.gameObject.SetActive(false);
    }
    public void UnPauseGame(){
        Time.timeScale = 1;
        if (MainMusicPlayer) MainMusicPlayer.volume = 1f;
        Pausemenu.gameObject.SetActive(false);
        // MainMusicPlayer.gameObject.SetActive(true);
    }
    public void returntoMenu(){
        SceneManager.LoadScene("MainMenu");
        Instance = null;
    }


    public void GameOverWin(){
        PauseGame();
        if (MainMusicPlayer) MainMusicPlayer.Pause();
        
        WinScreen.gameObject.SetActive(true);
    }

    public void GameOverLose(){
        PauseGame();
        if (MainMusicPlayer) MainMusicPlayer.Pause();

        LoseScreen.gameObject.SetActive(true);
    }



    public int _Pop;
    public int Pop {
        get {
            return _Pop;
        }
        set {    
            _Pop = value;
            populationText.text = _Pop + " / " + _Pop_cap;
        }
    }

    public int _Pop_cap;
    public int Pop_cap {
        get {
            return _Pop_cap;
        }
        set {    
            _Pop_cap = value;
            populationText.text = _Pop + " / " + _Pop_cap;
        }
    }

    public int _Food;
    public int Food {
        get {
            return _Food;
        }
        set {
            _Food = value;
            foodText.text = "" + _Food;
        }
    }


    public int _Material;
    public int Material {
        get {
            return _Material;
        }
        set {
            _Material = value;
            materialText.text = "" + _Material;
        }
    }
}


// void hideEvent(){
//     GameManager gm = GameManager.Instance;

//     GameManager.Instance.EventUI.SetActive(false)

//     gm.EventUI.gameObject.SetActive(false);
// }
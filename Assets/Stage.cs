using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    // Start is called before the first frame update

    public Resettle_cost resettle_cost;

    [SerializeField]
    public List<BuildingSpot> buildings;

    [SerializeField]
    public Quest[] quests;

    [SerializeField]
    public EventBase[] dayEvents;

    [SerializeField]
    public EventCost trickle;

    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void canResettle(){

    }

    public void triggerDayEvent(){
        GameManager gm = GameManager.Instance;

        // trickle
        gm.Pop += trickle.Pop;
        gm.Food += trickle.Food;
        gm.Material += trickle.Material;

        foreach(EventBase e in dayEvents){
            if (e.canTrigger()){
                // gm.PauseGame();
                gm.ShowEvent(e);
                return;
            }
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroductionEvent : EventBase
{

    // public string[] description;
    // public bool isChoice = true;
    // [SerializeField]
    // EventCost choice1, choice2;
    // public string choice1text = "Alright!", choice2text = "I got it!";

    // public Sprite EventSprite = Resources.Load("Sprites/aet.png") as Sprite;

    public IntroductionEvent(){
        title = "Welcome!";
        choice1text = "Alright!";
        EventSprite = Resources.Load("Sprites/aet.png") as Sprite;
    }


    override public string fulldescription {
        get {
            return @"You have run away from the city...

Once you reach a spot outside the city to setup camp, you will have to take care of the people with you.
The first thing to do is setup a fishing spot for FOOD.

Over the further course of your travels, you will have to make choices, some may impact your life along the river.

Manage your Population, provide Food for it and construct buildings with Materials.";
        }
        set {

        }
    }
}

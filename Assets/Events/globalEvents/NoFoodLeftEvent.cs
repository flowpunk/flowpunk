using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class NoFoodLeftEvent : EventBase
{
    // using EventBase {};

    public NoFoodLeftEvent(){
        title = "Not enough Food!";
        choice1text = "I need to be careful";
        // this.description = {"test"};
        
        EventSprite = Resources.Load("UI/food_icon.png") as Sprite;
    }

    override public string fulldescription {
        get {
            return "You have run out of food, without it, the people will not survive for long...";
        }
        set {

        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ResourceTrigger : MyScript
{
    // using EventBase {};
	[SerializeField]
	public EventCost triggerAbove;

    override public bool check(EventBase e){
        GameManager gm = GameManager.Instance;

        if (
            gm.Pop >= triggerAbove.Pop &&
            gm.Food >= triggerAbove.Food &&
            gm.Material >= triggerAbove.Material
        ) return true;
        return false;
    }

}

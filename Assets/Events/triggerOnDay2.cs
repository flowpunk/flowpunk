using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerOnDay2 : MyScript
{
    public int daytotrigger = 2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    override public bool check(EventBase e){
        // Debug.Log("trigger on day2 test");
        GameManager gm = GameManager.Instance;

        if (gm.dayOnSpot == daytotrigger) return true;

        return false;
    }
}

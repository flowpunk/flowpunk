using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using UnityEngine.Serialization;

using UnityEngine.Events;
using UnityEngine.EventSystems;

[System.Serializable]
public struct EventCost
{
    public int Pop;
    public int Food;
    public int Material;
}


[System.Serializable]
public class MyScript : MonoBehaviour {
    public virtual bool check(EventBase e){
        return false;
    }
}

[System.Serializable]
public class EventBase
{
    // [SerializeField]
    public string title;
    public List<string> description;
    public bool isChoice;
    [SerializeField]
    EventCost choice1, choice2;
    public string choice1text, choice2text;

    public Sprite EventSprite;

    public bool canStillTrigger = true;


    [SerializeField]
    public MyScript[] canTriggerArr;
    // private MyScript m_OnClick = new MyScript();

    public bool canTrigger(){
        Debug.Log("event canTrigger check");
        if (!canStillTrigger) return false; // only allow triggering if it hasn't been triggered before, i.e.
        foreach(MyScript fn in canTriggerArr){
            Debug.Log("checking script");
            Debug.Log(fn.check(this));
            if ( fn.check(this) ) return true;
        }
        return false;
    }

    void Awake(){
        
    }

    public void resolveEvent(bool accept){ // choice1 = true, choice2 = false
        canStillTrigger = false;

        Debug.Log("event " + this.title + "resolving"); 

        EventCost myCost;
        if (accept){
            myCost = choice1;
        } else {
            myCost = choice2;
        }

        GameManager gm = GameManager.Instance;

        gm.Pop += myCost.Pop;
        gm.Food += myCost.Food;
        gm.Material += myCost.Material;

        // gm.hideEvent();
    }

    public virtual string fulldescription {
        get {
            string myString = "";
            // return "test";
            if (null == description || 1 > description.Count) return "err";
            foreach(string s in this.description){
                myString += s + Environment.NewLine;
            }
            return myString;
        }
        set {

        }
    }
}

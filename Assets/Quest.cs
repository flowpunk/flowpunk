using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct QuestInfo {
    public string qName;
    public int dayTriggered;

    public int foodCost;
    public int materialCost;

    public string flavText;

    public Sprite sprite;
}


public class Quest : MonoBehaviour {
    public QuestInfo questinfo;

}

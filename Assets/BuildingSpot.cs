using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[System.Serializable]
public enum ProdType {
    None,
    Pop_cap,
    Food,
    Material
}

public class BuildingSpot : MonoBehaviour
{
    public GameObject productionPopupFood;
    public GameObject productionPopupMaterial;
    public Transform iconParent;
    public TMP_Text text;

    public string bName;
    public ProdType pType;

    public int prod_value;

    // public Sprite spriteEnabled;
    // public Sprite spriteDisabled;
    
    public int cost;

    public int prod_time;
    public float _speedMultiplyer;

    // private SpriteRenderer spriteRenderer;

    private bool isConstructed = false;

    // Start is called before the first frame update
    void Start()
    {
        // spriteRenderer = GetComponent<SpriteRenderer>();
        // BuildingConfig bc = GameManager.Instance.buildingConfig;
        // b = bc.GetBuilding(buildingType);

        gameObject.transform.Find("Construction").gameObject.SetActive(true);
        gameObject.transform.Find("Building").gameObject.SetActive(false);

        // spriteRenderer.sprite = spriteDisabled;
        text.text = "" + bName;

        // assign self to stage
        
        transform.parent.gameObject.GetComponent<Stage>().buildings.Add (this);
        
    }

    private float nextProdTime;
    // Update is called once per frame
    void Update() {

        if (isConstructed)
        {
            if(nextProdTime < Time.time) {
                nextProdTime += prod_time * _speedMultiplyer;
                Produce();
            }
        }
    }


    public void PlanConstruct(){
        if (!isConstructed) {
            gameObject.transform.Find("PopupWindow").gameObject.SetActive(true);

            gameObject.transform.Find("PopupWindow").Find("Panel").Find("Kosten").GetComponent<TMP_Text>().text = "" + this.cost;
            GameManager.Instance.PauseGame();
            return;
        };
        
    }

    public void CancelConstruct(){
        GameManager.Instance.UnPauseGame();
    }

    public void Construct()
    {
        
        GameManager.Instance.UnPauseGame();
        if (isConstructed) {
            return;
        };
        // gameObject.transform.Find("PopupWindow").gameObject.SetActive(false);

        isConstructed = true;
        GameManager gm = GameManager.Instance;
        if (gm.Material < cost) {
            // complain about not enough resources
            return;
        }
        gm.Material -= cost;
        
        nextProdTime = Time.time + prod_time * _speedMultiplyer;
        // spriteRenderer.sprite = spriteEnabled;

        gameObject.transform.Find("Construction").gameObject.SetActive(false);
        gameObject.transform.Find("Building").gameObject.SetActive(true);


        if (pType == ProdType.Pop_cap) {
            gm.Pop_cap += prod_value;
        }
    }

    private void Produce()
    {
        GameManager gm = GameManager.Instance;
        switch(pType){
            case ProdType.Food:
                gm.Food += prod_value;
                Instantiate(productionPopupFood, iconParent);
                break;
            case ProdType.Material:
                gm.Material += prod_value;
                Instantiate(productionPopupMaterial, iconParent);
                break;
            default: 
                break;
        }
    }
}
